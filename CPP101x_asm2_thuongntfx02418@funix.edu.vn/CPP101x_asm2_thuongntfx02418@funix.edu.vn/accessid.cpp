#include "accessid.h"

int n = 8;// So phan tu mang
int _Data[8] = { 8,5,4,1,2,3,6,7 };
int iData[8] = { 0 };
int Pass = 0;
//So sanh 2 mang
static int SoSanh(int a[], int b[]);
//Thay doi vi tri 2 phan tu
static void Swap(int& a, int& b);
//Nhap mang ma so vao
static void NhapMang();

static void SelectSort(int a[]);
static void BubbleSort(int a[]);

int _count = 0;
//Doi chieu 2 so
int Compare(int a, int b)
{
	_count += 1;
	return a - b;
}

static void Nhapmang()
{
	//int x;
	int ret = 0;
	while (ret != 1)
	{
		cout << "NHAP MA SO CA NHAN, MANG 08 SO" << endl;
		for (int i = 0; i < 8; i++)
		{
			cout << "NHAP SON THU " << i + 1 << " : ";
			cin >> iData[i];
			if (iData[i] > 9)
			{
				ret = 0;
				cout << "MOI NHAP LAI" << endl;
				break;
			}
			ret = 1;
		}
	}
}

static int SoSanh(int a[], int b[])
{
	int x = 1;
	for (int i = 0; i < 8; i++)
	{
		if (a[i] != b[i])
		{
			x = 0;
			break;
		}
	}
	return x;
}

//function selection Sort
static void SelectSort(int A[])
{
	for (int i = 0; i < n - 1; i++)
	{
		int m = i;
		for (int j = i + 1; j < n; j++)
		{
			if (Compare(A[j], A[m]) < 0)
			{
				m = j;
			}
		}
		if (m != i)
		{
			swap(A[i], A[m]);
		}
	}
}

//Function bubble soft
static void BubbleSort(int a[])
{
	for (int i = 0; i < n - 1; i++)
	{
		int c = 0;
		for (int j = n - 1; j > i; j--)
		{
			if (Compare(a[j], a[j - 1]) < 0)
			{
				swap(a[j], a[j - 1]);
				c = 1;
			}
		}
		if (c == 0) return;
	}
}

//Function Hoan doi
static void swap(int& a, int& b)
{
	a ^= b;
	b ^= a;
	a ^= b;
}

//Print
static void xuatmang(int a[])
{
	for (int i = 0; i < n; i++)
	{
		cout << "\t" << a[i];
	}
	cout << endl;
}
int Start()
{
	int i = 0;
	nhapmang();
	cout << "MANG NHAP VAO TRUOC KHI SAP XEP: ";
	xuatmang(iData);
	_count = 0;
	SelectSort(iData);
	cout << "SO LAN SO SANH: " << _count << endl;
	cout << "---> MANG NHAP VAO SAU KHI SAP XEP: ";
	xuatmang(iData);
	cout << endl;
	cout << "MA SO CA NHAN TRUOC KHI SAP XEP: ";
	xuatmang(_Data);
	_count = 0;
	BubbleSort(_Data);
	cout << "SO LAN SO SANH: " << _count << endl;
	cout << "---> MA SO CA NHAN SAU KHI SAP XEP: ";
	xuatmang(_Data);
	i = SoSanh(iData, _Data);
	return i;
}