#include "commands.h"

int Speed = 0;
int _Count = 0;
int reValue = 1;
int Data[8] = { 0 };
int SpeedWarning[3] = { 20, 50, 70 };

//In ra khoang cach canh bao
static void Waiting(int value);
//Menu tang giam toc do
static void Menu2();
//Tang giam toc do
static int SpeedCar();
//Toc do mac dinh khi bat dau
static void SpeedDefault();
//In khoang cach canh bao
static void DistanceWarning();

void Menu1()
{
	cout << "XIN MOI LUA CHON:" << endl;
	cout << "1. P" << endl;
	cout << "2. R" << endl;
	cout << "3. N" << endl;
	cout << "4. D" << endl;
	cout << "5. CAI DAT VAN TOC CANH BAO" << endl;
	cout << "6. POWER OFF" << endl;
	cout << ">>";
}
static void Menu2()
{
	cout << "MOI LUA CHON:" << endl;
	cout << "1. TANG TOC" << endl;
	cout << "2. GIAM TOC" << endl;
	cout << "3. PHANH" << endl;
	cout << "4. TRO VE MENU" << endl;
	cout << ">>";
}
//R: Lui/N:Tam dung/D:Tien/P:Do xe
int Command()
{
	char ret;
	cin >> ret;
	switch (ret)
	{
	case '1':
	case 'P':
	case 'p':
		if (Speed != 0)
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
		}
		else
		{
			system("cls");
			cout << "DA VE SO P." << endl;
			cout << "CHU Y SU SUNG PHANH TAY DE DAM BAO AN TOAN" << endl;
			cout << endl;
			Menu1();
		}
		reValue = 1;
		break;
	case '3':
	case 'N':
	case 'n':
		if (Speed != 0)
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
		}
		else
		{
			system("cls");
			cout << "DA VE SO N." << endl;
			cout << "CHU Y SU SUNG PHANH TAY DE DAM BAO AN TOAN" << endl;
			cout << endl;
			Menu1();
		}
		reValue = 1;
		break;
	case '2':
	case 'R':
	case 'r':
		_Count -= 1;
		reValue = 2;
		SpeedDefault();
		break;
	case '4':
	case 'D':
	case 'd':
		_Count += 1;
		reValue = 3;
		SpeedDefault();
		break;
	case '5':
		SetSpeedWarning();
		system("cls");
		cout << "SUCCESS!" << endl;
		Menu1();
		reValue = 1;
		break;
	case '6':
		if (Speed == 0)
		{
			cout << "HEN GAP LAI" << endl;
			reValue = 0;
		}
		else
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
			reValue = 1;
		}
		break;
	default:
		cout << "NHAP LAI YEU CAU CUA BAN" << endl;
		break;
	}
	return reValue;
}

static void SpeedDefault()
{
	int n;
	if (Speed == 0)
	{
		Speed = 7;
	}
	else if (_Count == 0 && Speed == 7)
	{
		Speed = 0;
	}
	while (true)
	{
		n = SpeedCar();
		if (n == 0)
		{
			break;
		}
	}
}

static int SpeedCar()
{
	int retValue = 1;
	char ret;
	if (reValue == 2 || reValue == 3)
	{
		Waiting(reValue);
		cin >> ret;
		switch (ret)
		{
		case '1':
			Speed += 5;
			Waiting(reValue);
			retValue = 1;
			break;
		case '2':
			Speed -= 5;
			Waiting(reValue);
			retValue = 1;
			break;
		case '3':
			Speed = 0;
			Waiting(reValue);
			retValue = 1;
			break;
		case '4':
			system("cls");
			Menu1();
			Command();
			retValue = 0;
			break;
		default:
			cout << "NHAP LAI YEU CAU CUA BAN" << endl;
			break;
		}
	}

	return retValue;
}

static void Waiting(int value)
{
	system("cls");
	if (value == 2 && Speed > 20)
	{
		if (Speed > 120)
		{
			Speed = 120;
		}
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << "TOC DO NGUY HIEM, BAN NEN GIEM TOC DO" << endl;
		cout << endl;
		Menu2();
	}
	else if (value == 3 && Speed > 60)
	{
		if (Speed > 120)
		{
			Speed = 120;
		}
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << "TOC DO NGUY HIEM, BAN NEN GIEM TOC DO" << endl;
		cout << endl;
		Menu2();
	}
	else if (Speed <= 0)
	{
		Speed = 0;
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << endl;
		Menu2();
	}
	else
	{
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << endl;
		Menu2();
	}
}

void SetSpeedWarning()
{
	SpeedWarning[0] = 0;
	SpeedWarning[1] = 0;
	SpeedWarning[2] = 0;
	system("cls");
	int ret = 1;
	while (ret)
	{
		cout << "CAI DAT VAN TOC CANH BAO KHONG CACH AN TOAN VOI 3 MUC VAN TOC (10, 20, ...)" << endl;
		for (int i = 0; i < 3; i++)
		{
			cout << "VAN TOC MUC DO " << i + 1 << ": ";
			cin >> SpeedWarning[i];
			if (SpeedWarning[i] % 10 != 0 || SpeedWarning[i] > 120)
			{
				system("cls");
				cout << "NHAP LAI VAN TOC CANH BAO" << endl;
				break;
			}
			if (i == 1 || i == 2)
			{
				if (SpeedWarning[i - 1] > SpeedWarning[i])
				{
					system("cls");
					cout << "NHAP LAI VAN TOC LON DAN" << endl;
					ret = 1;
					break;
				}
				else
				{
					ret = 0;
				}
			}
		}

	}
}

static void DistanceWarning()
{
	if (Speed <= SpeedWarning[0])
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 20m" << endl;
	}
	else if (Speed > SpeedWarning[0] && Speed <= SpeedWarning[1])
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 55m" << endl;
	}
	else
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 70m" << endl;
	}
}