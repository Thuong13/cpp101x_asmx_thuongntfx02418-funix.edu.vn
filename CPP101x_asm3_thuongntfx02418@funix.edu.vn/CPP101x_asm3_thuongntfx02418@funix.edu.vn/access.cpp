#include "access.h"

int access::WriteFile(const char* FileName)
{
	ifstream in(FileName, ios::in | ios::binary | ios::ate);
	int size = in.tellg();
	if (size <= 0)
	{
		in.close();
		cout << "XIN CHAO, HAY NHAP ID CHO LAN DAU TIEN!" << endl;
		ofstream out(FileName, ios::out | ios::binary);
		if (!out) {
			cout << "Cannot open file.";
			return 1;
		}
		NhapMang(_Data);
		XuatMang(_Data);
		out.write((char*)&_Data, sizeof _Data);
		out.close();
		in.close();
		cout << "SUCCESS!" << endl;
		system("pause");
		system("cls");
	}
	else
	{
		cout << "XIN CHAO!" << endl;
		in.close();
	}

	return 1;
}

void access::ReadFile(const char* FileName)
{
	ifstream in(FileName, ios::in | ios::binary);
	in.read((char*)&_Data, sizeof _Data);
	in.close();
}

//Doi chieu 2 so
int access::Compare(int a, int b)
{
	_count += 1;
	return a - b;
}

void access::NhapMang(int a[])
{
	int ret = 0;
	while (ret != 1)
	{
		cout << "NHAP MA SO CA NHAN, MANG 08 SO" << endl;
		for (int i = 0; i < 8; i++)
		{
			cout << "NHAP SON THU " << i + 1 << " : ";
			cin >> a[i];
			if (a[i] > 9)
			{
				ret = 0;
				cout << "MOI NHAP LAI" << endl;
				break;
			}
			ret = 1;
		}
	}
}

int access::SoSanh(int a[], int b[])
{
	int x = 1;
	for (int i = 0; i < 8; i++)
	{
		if (a[i] != b[i])
		{
			x = 0;
			break;
		}
	}
	return x;
}

//function selection Sort
void access::SelectSort(int A[])
{
	for (int i = 0; i < n - 1; i++)
	{
		int m = i;
		for (int j = i + 1; j < n; j++)
		{
			if (Compare(A[j], A[m]) < 0)
			{
				m = j;
			}
		}
		if (m != i)
		{
			swap(A[i], A[m]);
		}
	}
}

//Function bubble soft
void access::BubbleSort(int a[])
{
	for (int i = 0; i < n - 1; i++)
	{
		int c = 0;
		for (int j = n - 1; j > i; j--)
		{
			if (Compare(a[j], a[j - 1]) < 0)
			{
				swap(a[j], a[j - 1]);
				c = 1;
			}
		}
		if (c == 0) return;
	}
}

//Function Hoan doi
void access::Swap(int& a, int& b)
{
	a ^= b;
	b ^= a;
	a ^= b;
}

//Print
void access::XuatMang(int a[])
{
	for (int i = 0; i < n; i++)
	{
		cout << "\t" << a[i];
	}
	cout << endl;
}
int access::Start()
{
	int i = 0;
	const char* FileName = "AMS03.dat";
	WriteFile(FileName);
	ReadFile(FileName);
	NhapMang(iData);
	cout << "MANG NHAP VAO TRUOC KHI SAP XEP: ";
	XuatMang(iData);
	_count = 0;
	SelectSort(iData);
	cout << "SO LAN SO SANH: " << _count << endl;
	cout << "---> MANG NHAP VAO SAU KHI SAP XEP: ";
	XuatMang(iData);
	cout << endl;
	cout << "MA SO CA NHAN TRUOC KHI SAP XEP: ";
	XuatMang(_Data);
	_count = 0;
	BubbleSort(_Data);
	cout << "SO LAN SO SANH: " << _count << endl;
	cout << "---> MA SO CA NHAN SAU KHI SAP XEP: ";
	XuatMang(_Data);
	i = SoSanh(iData, _Data);
	return i;
}