#pragma once
#include <iostream>
#include<fstream>
#include <cstdio>
using namespace std;

class access
{
	int n = 8;// So phan tu mang
	int _Data[8] = { 0 };
	int iData[8] = { 0 };
	int Pass = 0;
	//Dem so lan doi chieu trong thuat toan
	int _count = 0;
public:
	int Start();
private:
	//So sanh 2 mang
	int SoSanh(int a[], int b[]);
	//Thay doi vi tri 2 phan tu
	void Swap(int& a, int& b);
	//Nhap mang ma so vao
	void NhapMang(int a[]);
	//Xuat mang
	void XuatMang(int a[]);
	void SelectSort(int a[]);
	void BubbleSort(int a[]);
	int Compare(int a, int b);
	//Write array ID in file
	int WriteFile(const char* FileName);
	//Read array ID from file
	void ReadFile(const char* FileName);
};

