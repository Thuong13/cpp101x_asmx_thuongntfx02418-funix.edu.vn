#include "commands.h"


void commands::Menu1()
{
	cout << "XIN MOI LUA CHON:" << endl;
	cout << "1. P" << endl;
	cout << "2. R" << endl;
	cout << "3. N" << endl;
	cout << "4. D" << endl;
	cout << "5. CAI DAT VAN TOC CANH BAO" << endl;
	cout << "6. POWER OFF" << endl;
	cout << ">>";
}
void commands::Menu2()
{
	cout << "MOI LUA CHON:" << endl;
	cout << "1. TANG TOC" << endl;
	cout << "2. GIAM TOC" << endl;
	cout << "3. PHANH" << endl;
	cout << "4. TRO VE MENU" << endl;
	cout << ">>";
}
//R: Lui/N:Tam dung/D:Tien/P:Do xe
int commands::Command()
{
	int reVal = 1;
	const char* FileSpeedWarning = "FileSpeedWarning.dat";

	WriteFile(FileSpeedWarning);
	ReadFile(FileSpeedWarning);
	system("cls");
	Menu1();
	char ret;
	cin >> ret;
	switch (ret)
	{
	case '1':
	case 'P':
	case 'p':
		if (Speed != 0)
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
		}
		else
		{
			system("cls");
			cout << "DA VE SO P." << endl;
			cout << "CHU Y SU SUNG PHANH TAY DE DAM BAO AN TOAN" << endl;
			cout << endl;
			Menu1();
		}
		reValue = 1;
		break;
	case '3':
	case 'N':
	case 'n':
		if (Speed != 0)
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
		}
		else
		{
			system("cls");
			cout << "DA VE SO N." << endl;
			cout << "CHU Y SU SUNG PHANH TAY DE DAM BAO AN TOAN" << endl;
			cout << endl;
			Menu1();
		}
		reValue = 1;
		break;
	case '2':
	case 'R':
	case 'r':
		_Count -= 1;
		reValue = 2;
		SpeedDefault();
		break;
	case '4':
	case 'D':
	case 'd':
		_Count += 1;
		reValue = 3;
		SpeedDefault();
		break;
	case '5':
		SetSpeedWarning();
		system("cls");
		cout << "SUCCESS!" << endl;
		Menu1();
		reValue = 1;
		break;
	case '6':
		if (Speed == 0)
		{
			cout << "HEN GAP LAI" << endl;
			reValue = 0;
		}
		else
		{
			system("cls");
			cout << "HAY CHAC CHAN XE DA DUNG VA VAN TOC LA 0 KM/H" << endl;
			cout << endl;
			Menu1();
			reValue = 1;
		}
		break;
	default:
		cout << "NHAP LAI YEU CAU CUA BAN" << endl;
		break;
	}
	return reValue;
}

void commands::SpeedDefault()
{
	int n;
	if (Speed == 0)
	{
		Speed = 7;
	}
	else if (_Count == 0 && Speed == 7)
	{
		Speed = 0;
	}
	while (true)
	{
		n = SpeedCar();
		if (n == 0)
		{
			break;
		}
	}
}

int commands::SpeedCar()
{
	int retValue = 1;
	char ret;
	if (reValue == 2 || reValue == 3)
	{
		Waiting(reValue);
		cin >> ret;
		switch (ret)
		{
		case '1':
			Speed += 5;
			Waiting(reValue);
			retValue = 1;
			break;
		case '2':
			Speed -= 5;
			Waiting(reValue);
			retValue = 1;
			break;
		case '3':
			Speed = 0;
			Waiting(reValue);
			retValue = 1;
			break;
		case '4':
			system("cls");
			Menu1();
			Command();
			retValue = 0;
			break;
		default:
			cout << "NHAP LAI YEU CAU CUA BAN" << endl;
			break;
		}
	}

	return retValue;
}

void commands::Waiting(int value)
{
	system("cls");
	if (value == 2 && Speed > 20)
	{
		if (Speed > 120)
		{
			Speed = 120;
		}
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << "TOC DO NGUY HIEM, BAN NEN GIEM TOC DO" << endl;
		cout << endl;
		Menu2();
	}
	else if (value == 3 && Speed > 60)
	{
		if (Speed > 120)
		{
			Speed = 120;
		}
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << "TOC DO NGUY HIEM, BAN NEN GIEM TOC DO" << endl;
		cout << endl;
		Menu2();
	}
	else if (Speed <= 0)
	{
		Speed = 0;
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << endl;
		Menu2();
	}
	else
	{
		cout << " ----  TOC DO HIEN TAI: " << Speed << " KM/H" << endl;
		DistanceWarning();
		cout << endl;
		Menu2();
	}
}

void commands::SetSpeedWarning()
{
	system("cls");
	int ret = 1;
	while (ret)
	{
		cout << "CAI DAT VAN TOC CANH BAO KHONG CACH AN TOAN VOI 3 MUC VAN TOC (10, 20, ...)" << endl;
		for (int i = 0; i < 3; i++)
		{
			cout << "VAN TOC MUC DO " << i + 1 << ": ";
			cin >> SpeedWarning[i];
			if (SpeedWarning[i] % 10 != 0 || SpeedWarning[i] > 120)
			{
				system("cls");
				cout << "NHAP LAI VAN TOC CANH BAO" << endl;
				break;
			}
			if (i == 1 || i == 2)
			{
				if (SpeedWarning[i - 1] > SpeedWarning[i])
				{
					system("cls");
					cout << "NHAP LAI VAN TOC LON DAN" << endl;
					ret = 1;
					break;
				}
				else
				{
					ret = 0;
				}
			}
		}

	}
}

void commands::DistanceWarning()
{
	if (Speed <= SpeedWarning[0])
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 20m" << endl;
	}
	else if (Speed > SpeedWarning[0] && Speed <= SpeedWarning[1])
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 55m" << endl;
	}
	else
	{
		cout << "BAN CHU Y GIU KHOANG CACH TOI THIEU LA 70m" << endl;
	}
}

int commands::WriteFile(const char* FileName)
{
	ifstream in(FileName, ios::in | ios::binary | ios::ate);
	int size = in.tellg();
	if (size <= 0)
	{
		in.close();
		cout << "XIN CHAO, HAY NHAP TOC DO CANH BAO" << endl;
		ofstream out(FileName, ios::out | ios::binary);
		if (!out) {
			cout << "Cannot open file.";
			return 1;
		}
		SetSpeedWarning();
		out.write((char*)&SpeedWarning, sizeof SpeedWarning);
		out.close();
		in.close();
		cout << "SUCCESS!" << endl;
		system("pause");
		system("cls");
	}
	else
	{
		in.close();
	}

	return 1;
}
void commands::ReadFile(const char* FileName)
{
	ifstream in(FileName, ios::in | ios::binary);
	in.read((char*)&SpeedWarning, sizeof SpeedWarning);
	in.close();
}