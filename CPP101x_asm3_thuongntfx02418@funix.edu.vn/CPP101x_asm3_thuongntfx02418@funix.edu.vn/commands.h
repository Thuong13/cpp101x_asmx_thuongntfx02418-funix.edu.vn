//#pragma once
#ifndef COMMANDS_H
#define COMMANDS_H

#include <iostream>
#include <fstream>
using namespace std;

class commands
{
	int Speed = 0;
	int _Count = 0;
	int reValue = 1;
	int Data[8] = { 0 };
	int SpeedWarning[3] = { 0 };

public:
	void SetSpeedWarning();
	void Menu1();
	int Command();
private:
	//In ra khoang cach canh bao
	void Waiting(int value);
	//Menu tang giam toc do
	void Menu2();
	//Tang giam toc do
	int SpeedCar();
	//Toc do mac dinh khi bat dau
	void SpeedDefault();
	//In khoang cach canh bao
	void DistanceWarning();
	//Write array speed warning in file
	int WriteFile(const char* FileName);
	//Read array speed warning from file
	void ReadFile(const char* FileName);
};

#endif // COMMANDS_H
