Visual Studio Community 2019 V16.5.5
Assignment 3 c++ cơ bản/CPP101x_03_VN_FX
Ý tưởng:
- Sử dụng lệnh rẽ nhánh "Switch case, if else" để lựa chọn thông số đầu vào
- Không có cấu trúc dặc biệt

Giải thích Một số function
- command:
	void Menu1(): menu chính
	void Waiting(int value): In ra khoang cach canh bao
	void Menu2(): Menu tang giam toc do
	int SpeedCar(): Tang giam toc do
	void SpeedDefault(): Toc do mac dinh khi bat dau
	void DistanceWarning(): In khoang cach canh bao
	int WriteFile(const char* FileName): Write array speed warning in file
	void ReadFile(const char* FileName): Read array speed warning from file

- accessid
	int SoSanh(int a[], int b[]): So sanh 2 mang
	void swap(int& a, int& b): Thay doi vi tri 2 phan tu
	void nhapmang(): Nhap mang ma so vao
	void SelectSort(int a[]): Thuật toán sắp xếp
	void BubbleSort(int a[]): Thuật toán sắp xếp
	int _count : Dem so lan so sanh trong thuật toán	
	int WriteFile(const char* FileName): Write array ID in file
	void ReadFile(const char* FileName): Read array ID from file